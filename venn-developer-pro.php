<?php
/**
 * Plugin Name: Venn Developer Pro
 * Plugin URI: https://venncreative.co.uk
 * Description: Making WordPress better for everyone
 * Author: Venn Creative
 * Version: 1.0
 * Author URI: https://venncreative.co.uk
 */

require __DIR__ . '/vendor/autoload.php';


/**
 * Prevent people editing code through the code editor
 */
if (!defined( 'DISALLOW_FILE_EDIT')) {
    define('DISALLOW_FILE_EDIT', true);
}

/**
 * Prevent users updating core, plugins and themes
 */
if (!defined('DISALLOW_FILE_MODS')) {
    define('DISALLOW_FILE_MODS', true);
}

function isAdmin()
{
    $roles = wp_get_current_user()->roles;

    return in_array('administrator', $roles);
}

// remove tools from Menu
add_action('admin_menu', function() {

    if (!isAdmin()) {
        remove_menu_page( 'tools.php' );
    }

});


// disable dashboard widgets
add_action('admin_init', function() {

    // remove wordpress news
    remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
    // remove quick draft
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );

});


// add dashboard widgets
// Function that outputs the contents of the dashboard widget
function dashboard_widget_function( $post, $callback_args ) {
    echo "Hello World, this is my first Dashboard Widget!";
}

// Function used in the action hook
function add_dashboard_widgets() {
    wp_add_dashboard_widget('dashboard_widget', 'Example Dashboard Widget', 'dashboard_widget_function');
}

// Register the new dashboard widget with the 'wp_dashboard_setup' action
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );


add_action('admin_init', function() {
    // get the the role object
    $role_object = get_role( 'editor' );

    // add $cap capability to this role object
    $role_object->add_cap( 'edit_theme_options' );
});
